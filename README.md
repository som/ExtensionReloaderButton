# Extension Reloader Button

A button to effectively reload a GNOME Shell extension.

It is just a user interface over the API provided by [ExtensionReloader](https://codeberg.org/som/ExtensionReloader).

## Usage

* From the menu:

  * Select the extension to reload.
  * Convenient access to the preferences of the last reloaded extension.
  <br>
* Once an extension have been reloaded:

  * `Ctrl + click` to reload again the same extension.
  * `Shift + click` to reload and open the preferences.
