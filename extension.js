/*
 * Copyright 2020 Abakkk
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-FileCopyrightText: 2020 Abakkk
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

/* jslint esversion: 8 */
/* exported init */

const { Clutter, Gio, GLib, GObject, St } = imports.gi;
const { main: Main, panelMenu: PanelMenu, popupMenu: PopupMenu } = imports.ui;

const getUuids = function() {
    let uuids = [];
    let path = GLib.build_filenamev([GLib.get_user_data_dir(), 'gnome-shell', 'extensions']);
    let dir = Gio.File.new_for_path(path);

    try {
        let enumerator = dir.enumerate_children('standard::name,standard::type', Gio.FileQueryInfoFlags.NONE, null);
        let info;

        while ((info = enumerator.next_file(null))) {
            if (info.get_file_type() != Gio.FileType.DIRECTORY)
                continue;
            if (!enumerator.get_child(info).get_child('metadata.json').query_exists(null))
                continue;
            uuids.push(info.get_name());
        }
    } catch (e) {}

    return uuids.sort((a, b) => a.localeCompare(b));
};

const Button = GObject.registerClass({
}, class Button extends PanelMenu.Button {
    _init(data) {
        super._init( 0.0, "Extension reloader button", false);
        this.menu.actor.add_style_class_name('extension-reloader-button-menu');

        let icon = new St.Icon({ icon_name: 'view-refresh-symbolic', style_class: 'system-status-icon' });
        this.add_actor(new St.Bin({ child: icon }));

        this._data = data;

        this._openPrefsItem = this.menu.addAction('', this._openExtensionPrefs.bind(this), 'emblem-system-symbolic');
        this._openPrefsItem.add_style_class_name('extension-reloader-button-ellipsized');
        this.menu.addMenuItem(new PopupMenu.PopupSeparatorMenuItem());

        this._recentsSubMenuItem = new PopupMenu.PopupSubMenuMenuItem('Recents', true);
        this._recentsMenu = this._recentsSubMenuItem.menu;
        this._recentsMenu.actor.add_style_class_name('extension-reloader-button-ellipsized');
        this.menu.addMenuItem(this._recentsSubMenuItem);

        let allSubMenuItem = new PopupMenu.PopupSubMenuMenuItem('All', true);
        this._allMenu = allSubMenuItem.menu;
        this._allMenu.actor.add_style_class_name('extension-reloader-button-ellipsized');
        this.menu.addMenuItem(allSubMenuItem);
    }

    get uuid() {
        return this._data.uuid;
    }

    set uuid(uuid) {
        this._data.uuid = uuid;
        this._data.recents.set(uuid, Date.now());
        this._reload();
    }

    _openExtensionPrefs() {
        this._data.openExtensionPrefsFunction();
    }

    _openMenu() {
        this._openPrefsItem.label.text = this.uuid || "No extension selected";
        this._openPrefsItem.reactive = this._data.openExtensionPrefsFunction ? true : false;

        this._recentsMenu.removeAll();
        [...this._data.recents.keys()].sort((a, b) => this._data.recents.get(a) < this._data.recents.get(b)).forEach(uuid => {
            let subItem = this._recentsMenu.addAction(uuid, () => this.uuid = uuid);
            subItem.connect('key-focus-in', this._updateSubMenuAdjustment.bind(null, subItem));
        });
        this._recentsSubMenuItem.reactive = !this._recentsMenu.isEmpty();

        this._allMenu.removeAll();
        getUuids().forEach(uuid => {
            let subItem = this._allMenu.addAction(uuid, () => this.uuid = uuid);
            subItem.connect('key-focus-in', this._updateSubMenuAdjustment.bind(null, subItem));
        });

        if (this._recentsMenu.numMenuItems >= 1)
            this._recentsMenu.open();

        this.menu.toggle();
    }

    _reload(openPrefs) {
        this.remove_style_class_name('extension-reloader-button-error');
        this._data.openExtensionPrefsFunction = null;

        if (!this.uuid)
            return Main.notify("No extension selected");

        try {
            if (!Main.extensionManager.trulyReloadExtension)
                throw new Error("Extension Reloader is not enabled. See the README.");

            let [newUuid, openExtensionPrefsFunction] = Main.extensionManager.trulyReloadExtension(this.uuid);
            Main.notify("Extension reloaded", newUuid);
            this._data.openExtensionPrefsFunction = openExtensionPrefsFunction;
            if (openPrefs)
                openExtensionPrefsFunction();
        } catch(e) {
            Main.notifyError('Failed', e.message);
            this.add_style_class_name('extension-reloader-button-error');
        }
    }

    vfunc_event(event) {
        if (event.type() == Clutter.EventType.BUTTON_PRESS || event.type() == Clutter.EventType.TOUCH_BEGIN) {
            if (event.has_control_modifier())
                this._reload(false);
            else if (event.has_shift_modifier())
                this._reload(true);
            else
                this._openMenu();

            return Clutter.EVENT_STOP;
        }

        return Clutter.EVENT_PROPAGATE;
    }

    _onEvent() {
        // Override _onEvent for GS 3.34
    }

    _updateSubMenuAdjustment(item) {
        let scrollView = item.get_parent().get_parent();
        let adjustment = scrollView.get_vscroll_bar().get_adjustment();
        let scrollViewAlloc = scrollView.get_allocation_box();
        let currentScrollValue = adjustment.get_value();
        let height = scrollViewAlloc.y2 - scrollViewAlloc.y1;
        let itemAlloc = item.get_allocation_box();
        let newScrollValue = currentScrollValue;
        if (currentScrollValue > itemAlloc.y1 - 10)
            newScrollValue = itemAlloc.y1 - 10;
        if (height + currentScrollValue < itemAlloc.y2 + 10)
            newScrollValue = itemAlloc.y2 - height + 10;
        if (newScrollValue != currentScrollValue)
            adjustment.set_value(newScrollValue);
    }

    _onOpenStateChanged(menu, open) {
        if (open)
            this.add_style_pseudo_class('active');
        else
            this.remove_style_pseudo_class('active');

        let workArea = Main.layoutManager.getWorkAreaForMonitor(Main.layoutManager.primaryIndex);
        let scaleFactor = St.ThemeContext.get_for_stage(global.stage).scale_factor;
        let maxHeight = Math.round(0.85 * workArea.height / scaleFactor);
        this.menu.actor.style = 'max-height: %spx;'.format(maxHeight);
    }
});

class Extension {
    constructor() {
    }

    enable() {
        // Some data need to be stored outside the extension because reloading an extension may cause this extension to be reloaded too.
        if (!Main.extensionManager.extensionReloaderButtonData)
            Main.extensionManager.extensionReloaderButtonData = { uuid: '', recents: new Map(), openExtensionPrefsFunction: null };
        this.button = new Button(Main.extensionManager.extensionReloaderButtonData);
        Main.panel.addToStatusArea('extensionReloaderButton', this.button);
    }

    disable() {
        this.button.destroy();
    }
}

function init() {
    return new Extension();
}
